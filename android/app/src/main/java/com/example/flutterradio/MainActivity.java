package com.example.flutterradio;
import android.app.AlarmManager;
import java.text.SimpleDateFormat;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Handler;
import android.os.Build;
import android.widget.Toast;
import androidx.annotation.NonNull;
import java.util.Calendar;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class MainActivity extends FlutterActivity {
  FlutterEngine flutterEngine;
  Timer timer = new Timer();

  TimerTask timerTask;
  final Handler handler = new Handler();
  private static final String channel = "mensaje_nativo";

  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {

    GeneratedPluginRegistrant.registerWith(flutterEngine);

    new MethodChannel(getFlutterEngine().getDartExecutor().getBinaryMessenger(),"com.flutter.background_services").setMethodCallHandler(new MethodChannel.MethodCallHandler() {
    
      @Override
      public void onMethodCall(MethodCall call, MethodChannel.Result result) {

      if(call.method.equals("setRadioAlarma")){
          long time = call.argument("time");
          setRadioAlarma(time);
           result.success("services started");
        }
      }
     }
    );
  
    new MethodChannel(getFlutterEngine().getDartExecutor().getBinaryMessenger(),"canal_temporizador").setMethodCallHandler(
      new MethodChannel.MethodCallHandler() {
  
      @Override
      public void onMethodCall(MethodCall call, MethodChannel.Result result) {

      if(call.method.equals("temp_nativo")){
          int time = call.argument("time");
          temp_nativo(time);
           result.success("services stopped");
        }
       }
      }
    );
   }

   private void setRadioAlarma(long time) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.setAction(Intent.ACTION_MAIN);
    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    PendingIntent pi = PendingIntent.getActivity(this, 0,
            intent, 0);
    AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        am.set(AlarmManager.RTC, time,pi);


    //metodo 2
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY,hour);
//        calendar.set(Calendar.MINUTE,minute);
    am.setRepeating(AlarmManager.RTC_WAKEUP,time,AlarmManager.INTERVAL_DAY,pi);


    Toast.makeText(this, "Alarma Programada", Toast.LENGTH_SHORT).show();
}

  //  private void setRadioAlarma(long time) {
  //     Intent intent = new Intent(this, MainActivity.class);
  //     intent.setAction(Intent.ACTION_MAIN);
  //     PendingIntent pi = PendingIntent.getActivity(this, 0,intent, 0);
  //     AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
  //     am.setRepeating(AlarmManager.RTC_WAKEUP,time,AlarmManager.INTERVAL_DAY,pi);
  //     MethodChannel methodChannel = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), "com.example.importfluttermodule/data");
  //     methodChannel.invokeMethod("passToFlutter",time);
  // }

    public void temp_nativo(int time) { 
      timer = new Timer();
      tarea_tem(time);
      timer.schedule(timerTask,time);
    Toast.makeText(this, "Temporizador Programado", Toast.LENGTH_SHORT).show();
    }

    public void tarea_tem(int time){
    timerTask = new TimerTask() {
      public void run() {
          handler.post(new Runnable() {
              public void run() { 
                  Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  intent.putExtra("EXIT", true);
                  startActivity(intent);
                  finish();
                  System.runFinalization();
                  System.exit(0);
              }
          }
        );
      }
    };
  }
}
