import 'package:flutter/material.dart';
import 'package:flutterradio/SplashScreen/cargador.dart';
import 'package:global_configuration/global_configuration.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("roa");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: GlobalConfiguration().getString("title"),
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: Loading(),
    );
  }
}
