import 'package:flutter/material.dart';
import 'package:flutterradio/HomePage.dart';
import 'package:flutterradio/fondo_color/hex_color.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:loading_indicator/loading_indicator.dart';
// contiene el loading animado al iniciar la radio.
class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    Future.delayed(
      Duration(seconds: 2),
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => SafeArea(child: MyHomePage()),
        ),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 50,
              height: 50,
              child: LoadingIndicator(
                indicatorType: Indicator.ballRotateChase,
                color: HexColor(GlobalConfiguration().getString("color")),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

