import 'package:flutter/material.dart';
import 'package:flutterradio/fondo_color/hex_color.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:webview_flutter/webview_flutter.dart';
// muestra la pagina web de la radio
class NoticiaWebView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var link = "https://radiocarabaya.com";
    // var link = "https://radioondaazul.com";
    var link = GlobalConfiguration().getString('paginaweb');
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(GlobalConfiguration().getString("title")),
          backgroundColor: HexColor(GlobalConfiguration().getString("color")),
        ),
        body: WebView(initialUrl: link, javascriptMode: JavascriptMode.unrestricted,));
  }
}
