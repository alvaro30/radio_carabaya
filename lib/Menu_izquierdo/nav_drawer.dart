// import 'dart:io';
// import 'package:audio_service/audio_service.dart';
// import 'dart:io';

import 'dart:io';

import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterradio/SplashScreen/loading.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:fontisto_flutter/fontisto_flutter.dart';
import 'dart:convert';
// clase del menu izquierdo 
class NavDrawer extends StatefulWidget {
  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  double icono = 0;
  ListTile hola;
  List<dynamic> _posts;
  String telefono = "";
  String telefono1 = GlobalConfiguration().getString("telefono");
  String mensaje1 = GlobalConfiguration().getString("mensaje");
  String whatsapp = GlobalConfiguration().getString("whatsapp");
  bool icono1 = false;
  @override
  void initState() {
    super.initState();
    this._setPosts();
  }

  Future<List<dynamic>> getListFromConfig(String filename) async {
    final contents = await rootBundle.loadString('assets/cfg/' + filename);
    return json.decode(contents);
  }

  void _setPosts() async {
    getListFromConfig('carabaya.json').then((response) {
      setState(() {
        String temp;
        this._posts = response;
        _posts.map((e) {
          temp = e['telefono'];
        });
        telefono = temp;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/img/' +
                        GlobalConfiguration().getString("background")))),
            child: null,
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Inicio'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.language),
            title: Text('Sitio Web'),
            onTap: () => {navWebsite(context)},
          ),
          this.enviarmensaje(),
          ListTile(
            leading: Icon(Istos.whatsapp),
            title: Text('Enviar whatsapp'),
            onTap: () {
              Navigator.of(context).pop(FlutterOpenWhatsapp.sendSingleMessage(
                  whatsapp,
                  "Hola " + GlobalConfiguration().getString("title") + ": "));
            },
          ),

          this.llamarRadio(),
          ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Salir'),
              onTap: () {
                Navigator.of(context).pop(salir());
              }),
        ],
      ),
    );
  }

  void navWebsite(BuildContext context) {
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SplashScreen(),
      ),
    );
  }

  Widget llamarRadio() {
    return ListTile(
      leading: Icon(Icons.call),
      title: Text('Llamar a la radio'),
      onTap: () {
        Navigator.of(context).pop(llamar(telefono1));
      },
    );
  }

  Widget enviarmensaje() {
    return ListTile(
      leading: Icon(Icons.message),
      title: Text('Enviar mensaje'),
      onTap: () {
        Navigator.of(context).pop(sms(mensaje1));
      },
    );
  }

  Widget enviarwhatsapp() {
    return ListTile(
      leading: Icon(Icons.call),
      title: Text('Llamar a la radio'),
      onTap: () {
        Navigator.of(context).pop(llamar(telefono1));
      },
    );
  }

  sms(String mensaje) {
    launch(mensaje);
  }

  llamar(String llamar) {
    launch(llamar);
  }

  salir() {
    AudioService.stop();
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    exit(0);
  }
}
