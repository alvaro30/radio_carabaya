import 'dart:async';
import 'package:audio_service/audio_service.dart';
import 'package:circular_menu/circular_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterradio/Menu_izquierdo/nav_drawer.dart';
import 'package:flutterradio/fondo_color/fondo.dart';
import 'package:flutterradio/menu_derecho/slider_dialog.dart';
import 'package:flutterradio/radio/audio_services.dart';
import 'package:flutterradio/radio/gif.dart';
import 'package:flutterradio/radio/radio_player.dart';
import 'dart:io' show Platform;
import 'package:global_configuration/global_configuration.dart';
import 'package:flutterradio/fondo_color/hex_color.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:launch_review/launch_review.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:day_night_time_picker/day_night_time_picker.dart';
import 'package:day_night_time_picker/lib/constants.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  GifMusica gif2 = new GifMusica();
  bool activo = false;
  int resto = 10;
  String msg =
      'https://play.google.com/store/apps/details?id=com.gruposistemas.radio_onda_azul';
  AnimationController _animationController;
  TimeOfDay _tiempo = TimeOfDay.now();

  void onTimeChanged(TimeOfDay newTime) {
    setState(() {
      _tiempo = newTime;
    });
  }
// funcion que hace cpnexion con el nativo para alarma
  void startServiceInPlatform(DateTime _dateTime) async {
    if (Platform.isAndroid) {
      var methodChannel = MethodChannel(
        "com.flutter.background_services",
      );

      DateTime time = new DateTime(
        _dateTime.year,
        _dateTime.month,
        _dateTime.day,
        _dateTime.hour,
        _dateTime.minute,
        //20
        -30,
        -30,
        -30,
      );

      await methodChannel.invokeMethod(
          "setRadioAlarma", {"time": time.millisecondsSinceEpoch});
    }
  }

  PageController pageController =
      PageController(initialPage: 0, keepPage: true);
  Widget buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {},
      children: <Widget>[Principal()],
    );
  }
// modal de calificacion ala app
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        await rateMyApp.init();
        if (mounted && rateMyApp.shouldOpenDialog) {
          rateMyApp.showStarRateDialog(
            context,
            title: GlobalConfiguration().getString("title"),
            message: 'Te gusta esta aplicacion? brindenos su valoracion:',
            actionsBuilder: (context, stars) {
              return [
                FlatButton(
                  child: Text(
                    'Enviar',
                    style: TextStyle(
                      color: HexColor(GlobalConfiguration().getString("color")),
                    ),
                  ),
                  onPressed: () async {
                    debugPrint('Gracias por ' +
                        (stars == null ? '0' : stars.round().toString()) +
                        ' estrellas !');
                    await rateMyApp
                        .callEvent(RateMyAppEventType.rateButtonPressed);
                    Navigator.pop<RateMyAppDialogButton>(
                        context, RateMyAppDialogButton.rate);
                  },
                ),
              ];
            },
            ignoreNativeDialog: Platform.isAndroid,
            dialogStyle: DialogStyle(
              titleAlign: TextAlign.center,
              messageAlign: TextAlign.center,
              messagePadding: EdgeInsets.only(bottom: 20),
            ),
            starRatingOptions: StarRatingOptions(),
            onDismissed: () =>
                rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed),
          );
        }
      },
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      print(
          "===========================================================================0resumed");
      activo = true;
    } else if (state == AppLifecycleState.inactive) {
      print(
          "===========================================================================inactivo");
      activo = true;
    } else if (state == AppLifecycleState.detached) {
      RadioPlayer().methodstop();
      print(
          "===========================================================================detachatable");
    } else if (state == AppLifecycleState.paused) {
      print(
          "===========================================================================pausado");
      activo = true;
    }
  }
// para la frecuencia del modal calificar .
  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 1,
    minLaunches: 1,
    remindDays: 1,
    remindLaunches: 1,
    googlePlayIdentifier: 'com.gruposistemas.radio_onda_azul',
    //appStoreIdentifier: '1491556149',
  );

  empezar() {
    AudioService.start(
      backgroundTaskEntrypoint: _iniciarRadio,
      androidNotificationChannelName: 'Audio Service Demo',
      androidNotificationColor: 4280830081,
      androidNotificationIcon: 'mipmap/logo_radio',
      androidEnableQueue: true,
      androidResumeOnClick: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(GlobalConfiguration().getString("title")),
        backgroundColor: HexColor(GlobalConfiguration().getString("color")),
        actions: <Widget>[
          // abre el menu derebro
          PopupMenuButton<int>(
            onSelected: (value) {
              // temporizador
              if (value == 1) {
                showDialog(
                  context: context,
                  builder: (context) => TimePickerDialog(initialtime: 12),
                );
              } else {
                // alarma
                Navigator.of(context).push(
                  showPicker(
                    context: context,
                    unselectedColor:
                        HexColor(GlobalConfiguration().getString("color")),
                    accentColor:
                        HexColor(GlobalConfiguration().getString("color")),
                    value: _tiempo,
                    onChange: onTimeChanged,
                    minuteInterval: MinuteInterval.ONE,
                    disableHour: false,
                    disableMinute: false,
                    cancelText: "Cancelar",
                    okText: "Guardar",
                    minMinute: 0,
                    maxMinute: 59,
                    onChangeDateTime: (DateTime _dateTime) {
                      startServiceInPlatform(_dateTime);
                      DateTime hola1 = DateTime.now();
                      Duration minutos2;
                      minutos2 = _dateTime.difference(hola1);
                      Timer(
                          Duration(
                            hours: minutos2.inHours,
                            // minutes: minutos2.inMinutes,
                            seconds: minutos2.inSeconds,
                            milliseconds: 0,
                            microseconds: 0,
                          ), () async {
                        RadioPlayer().methodplay();

                        // if (AudioService.running) {
                        //   AudioService.play();
                        // } else {
                        //   await empezar();
                        // }
                      });
                    },
                  ),
                );
              }
            },
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                value: 1,
                child: Container(
                  child: Row(
                    children: [
                      Icon(
                        Icons.timer,
                        color: Colors.black,
                      ),
                      SizedBox(width: 5),
                      Text("Temporizador"),
                    ],
                  ),
                ),
              ),
              PopupMenuItem(
                value: 2,
                child: Container(
                  child: Row(
                    children: [
                      Icon(
                        Icons.alarm_add_sharp,
                        color: Colors.black,
                      ),
                      SizedBox(width: 5),
                      Text("Alarma de radio"),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      // parte inferior de la radio boton play y titulos animados
      body: Container(
        child: new Stack(
          children: <Widget>[
            buildPageView(),
            Positioned(
              child: new Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: AudioServiceWidget(child: RadioPlayer(gif2:gif2))),
            ),
            CircularMenu(
              toggleButtonAnimatedIconData: AnimatedIcons.add_event,
              alignment: Alignment.bottomRight,
              toggleButtonSize: 25,
              toggleButtonColor:
                  HexColor(GlobalConfiguration().getString("fontColor1")),
              startingAngleInRadian: 3.14159,
              endingAngleInRadian: 4.71239,
              items: [
                CircularMenuItem(
                    icon: Icons.star_outline,
                    color: HexColor(GlobalConfiguration().getString("color")),
                    onTap: () {
                      this.calificar();
                      _animationController.reverse();
                    }),
                CircularMenuItem(
                  icon: Icons.share_outlined,
                  color: HexColor(GlobalConfiguration().getString("color")),
                  onTap: () async {
                    var response =
                        await FlutterShareMe().shareToSystem(msg: msg);
                    if (response == 'success') {
                      print('navigate success');
                      _animationController.reverse();
                    } else {
                      print("no tiene whatsapp");
                    }
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void calificar() {
    LaunchReview.launch(
        androidAppId: "com.gruposistemas.radio_onda_azul",
        iOSAppId: "585027354");
  }
}

void _iniciarRadio() async {
  AudioServiceBackground.run(() => AudioPlayerTask());
}
