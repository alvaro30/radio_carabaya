import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutterradio/radio/audio_services.dart';
import 'package:flutterradio/radio/gif.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:flutterradio/fondo_color/hex_color.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
// import 'package:loading_indicator/loading_indicator.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// clase de la radio de la parte inferior  icono play y stop y los titulos animados 
class RadioPlayer extends StatefulWidget {
  GifMusica gif2;
  RadioPlayer({this.gif2});
  methodstop() => createState().stop();
  methodplay() => createState().play();
  methodpause() => createState().pause();
  hola() => createState().hola();

  @override
  _RadioPlayerState createState() => _RadioPlayerState();
}

class _RadioPlayerState extends State<RadioPlayer> with WidgetsBindingObserver {
  GifMusica _gif= new GifMusica();
  bool activo = false;

  IconData iconplayer = FontAwesomeIcons.play;
  double safrono = 30;
  double opacityGif = 0;
  double loader = 0;
  int colornum = 4293322470;
  bool cargando = false;
  String colorjson = "";
  @override
  void initState() {
    super.initState();
   
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = 10;
    if (MediaQuery.of(context).orientation == Orientation.landscape) {
      screenHeight = MediaQuery.of(context).size.height * 0.20;
    } else {
      screenHeight = MediaQuery.of(context).size.height * 0.10;
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
         Container(
                  child: StreamBuilder<PlaybackState>(
                    stream: AudioService.playbackStateStream,
                    builder: (context, snapshot) {
                      final playing2 = snapshot.data?.playing ?? false;
// gif de audio 
                      return   playing2 == true
                              ? Opacity(
                                opacity: 1.0,
                                child: Image.asset("assets/img/voz.gif", gaplessPlayback: true)
                              )
                              : Container();
                    },
                  ),
                ),
          Container(
            height: screenHeight,
            decoration: BoxDecoration(
              color: HexColor(GlobalConfiguration().getString("color"))
                  .withOpacity(0.7),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 10.0,
                  offset: Offset(0.0, 5.0),
                ),
              ],
            ),
            child: Row(
              children: [
                Container(
                  child: StreamBuilder<PlaybackState>(
                    stream: AudioService.playbackStateStream,
                    builder: (context, snapshot) {
                      final playing = snapshot.data?.playing ?? false;

                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          playing == true
                              ? IconButton(
                                  icon: Icon(FontAwesomeIcons.stop),
                                  iconSize: 30,
                                  color: Colors.white,
                                  onPressed: stop,
                                )
                              // : cargando == true
                              //     ? Center(
                              //         child: Container(
                              //           padding: EdgeInsets.all(8),
                              //           width: 50,
                              //           height: 50,
                              //           child: LoadingIndicator(
                              //             indicatorType:
                              //                 Indicator.ballRotateChase,
                              //             color: Colors.white,
                              //           ),
                              //         ),
                              //       )
                                  : IconButton(
                                      icon: Icon(iconplayer),
                                      iconSize: safrono,
                                      color: Colors.white,
                                      onPressed: play,
                                    ),
                        ],
                      );
                    },
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      GlobalConfiguration().getString("title"),
                      style: TextStyle(
                        color: HexColor(
                            GlobalConfiguration().getString("fontColor1")),
                      ),
                    ),
                    TypewriterAnimatedTextKit(
                      speed: Duration(milliseconds: 50),
                      text: [
                        GlobalConfiguration().getString("subtitle"),
                        GlobalConfiguration().getString("subtitle2"),
                      ],
                      textStyle: TextStyle(
                        fontSize: 14.0,
                        fontFamily: "Agne",
                        color: HexColor(
                            GlobalConfiguration().getString("fontColor2")),
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
// llamada ala notiicacion y su respectivo color de ello.
  empezar() {
    AudioService.start(
      backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
      androidNotificationChannelName: 'Audio Service Demo',
      androidNotificationColor: 4280830081,
      androidNotificationIcon: 'mipmap/logo_radio',
      androidEnableQueue: true,
      androidResumeOnClick: true,
    ).whenComplete(() {
      setState(() {
        GifMusica().setOpacity(1);
        // widget.gif2.setOpacity(1);
        //  _gif.setOpacity(1);
        opacityGif = 1;
      });
    });
  }

  play() {
    if (AudioService.running) {
      AudioService.play();
      setState(() {
        GifMusica().setOpacity(1);
        // widget.gif2.setOpacity(1);
        // _gif.setOpacity(1);
        opacityGif = 1;
        loader = 0;
        iconplayer = FontAwesomeIcons.play;
      });
    } else {
      empezar();
      setState(() {
        //  _gif.setOpacity(0);
        // widget.gif2.setOpacity(0);
        GifMusica().setOpacity(0);
        opacityGif = 0;
        loader = 1;
        // iconplayer = FontAwesomeIcons.spinner;
        safrono = 25;
        cargando = true;
      });
    }
  }

  hola() {
    setState(() {
      opacityGif = 0;
    });
  }

  pause() {
    AudioService.stop();
    // _gif.setOpacity(0);
    // widget.gif2.setOpacity(0);
    GifMusica().setOpacity(0);
    setState(() {
      opacityGif = 0;
      GifMusica().setOpacity(0);
    
    });
  }
  //   pause() {
  //   AudioService.pause();
  //   setState(() {
  //     opacityGif = 0;
  //     loader = 0;
  //     iconplayer = FontAwesomeIcons.play;
  //     safrono = 30;
  //     cargando = false;
  //   });
  // }


  stop() {
    GifMusica().setOpacity(0);
    // widget.gif2.setOpacity(0);
    AudioService.stop();
    setState(() {
      //  _gif.setOpacity(0);
      // widget.gif2.setOpacity(0);
      GifMusica().setOpacity(0);
      opacityGif = 0;
      loader = 0;
      iconplayer = FontAwesomeIcons.play;
      safrono = 30;
      cargando = false;
    });
  }

  // stop() => AudioService.stop();
}

void _audioPlayerTaskEntrypoint() async {
  AudioServiceBackground.run(() => AudioPlayerTask());
}
