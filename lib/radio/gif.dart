import 'package:flutter/material.dart';
class GifMusica {
  double opacityGif;
  GifMusica(){this.opacityGif=0;}

  Widget getgif(){
    return  Opacity(
            opacity: opacityGif,
            child: Image.asset("assets/img/voz.gif", gaplessPlayback: true)
          );
  }

  void setOpacity(double numero){
    opacityGif=numero;
  }
}