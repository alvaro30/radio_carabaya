import 'dart:async';
import 'package:flutterradio/radio/radio_player.dart';
import 'package:just_audio/just_audio.dart';
import 'package:audio_service/audio_service.dart';
// plugin de la radio de segundo plano(notificacion)
class AudioPlayerTask extends BackgroundAudioTask {

  
  final _mediaLibrary = MediaLibrary();
  AudioPlayer _player = new AudioPlayer();
  AudioProcessingState _skipState;
  StreamSubscription<PlaybackEvent> _eventSubscription;
  List<MediaItem> get queue => _mediaLibrary.items;
  int get index => _player.currentIndex;
  MediaItem get mediaItem => index == null ? null : queue[index];

  @override
  Future<void> onStart(Map<String, dynamic> params) async {
    // final session = await AudioSession.instance;
    // await session.configure(AudioSessionConfiguration.speech());
    _player.currentIndexStream.listen((index) {
      if (index != null) AudioServiceBackground.setMediaItem(queue[index]);
    });
    _eventSubscription = _player.playbackEventStream.listen((event) {
      _broadcastState();
    });
    _player.processingStateStream.listen((state) {
      switch (state) {
        case ProcessingState.completed:
          onStop();
          break;
        case ProcessingState.ready:
          _skipState = null;
          break;
        default:
          break;
      }
    });

    AudioServiceBackground.setQueue(queue);
    try {
      await _player.setAudioSource(ConcatenatingAudioSource(
        children:
            queue.map((item) => AudioSource.uri(Uri.parse(item.id))).toList(),
      ));
      onPlay();
    } catch (e) {
      print("Error: $e");
      onStop();
    }
  }
pause2(){
  _player.pause();
   RadioPlayer().methodpause();

  //  _gif.setOpacity(0);
}
  @override
  Future<void> onPlay() => _player.play();

  @override
  Future<void> onPause() => pause2();

  @override
  Future<void> onSeekTo(Duration position) => _player.seek(position);

  @override
  Future<void> onStop() async {
    await _player.dispose();
    _eventSubscription.cancel();
    await _broadcastState();
    await super.onStop();
  }

  Future<void> _broadcastState() async {
    await AudioServiceBackground.setState(
      controls: [
        if (_player.playing) MediaControl.pause else MediaControl.play,
        MediaControl.stop,
      ],
      systemActions: [
        MediaAction.seekTo,
        MediaAction.seekForward,
        MediaAction.seekBackward,
      ],
      androidCompactActions: [0, 1],
      playing: _player.playing,
      position: _player.position,
      bufferedPosition: _player.bufferedPosition,
      speed: _player.speed,
    );
  }
}

class MediaLibrary {
  final _items = <MediaItem>[
    MediaItem(
      id: "http://167.114.118.120:7160",
      album: "640 AM - 95.7 FM",
      title: "Radio Onda Azul",
      // artist: "808 AM",
      duration: Duration(milliseconds: 5739820),
      artUri:
          "https://4.bp.blogspot.com/-FckBk1weJEQ/WJiC-po6VzI/AAAAAAAADsg/pV8r2aCF_vkEx744rwFYmhbUw2hp0SkrACLcB/s1600/radio-onda-azul.jpg",
    ),
  ];

  List<MediaItem> get items => _items;
}
